import React from 'react';
import {Navigation} from './src/navigations';

const App = () => {
  return (
    <React.Fragment>
      <Navigation />
    </React.Fragment>
  );
};

export default App;
