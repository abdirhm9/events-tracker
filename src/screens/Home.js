import {useFocusEffect} from '@react-navigation/native';
import React, {useState, useEffect} from 'react';
import {View, Text, StatusBar, Image, BackHandler} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import normalize from 'react-native-normalize';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Card from '../components/organisms/Card';
import TabBar from '../components/organisms/TabBar';
import {useGetObject, useSetObject} from '../hooks';
import {data} from '../utils/Data';

const Home = ({navigation, route}) => {
  const {activeUser} = route.params;
  const [isGridView, setIsGridView] = useState(false);

  useEffect(() => {
    let mounted = true;
    mounted && getView();
    return () => (mounted = false);
  }, []);

  useFocusEffect(() => {
    const handleBack = () => {
      return BackHandler.exitApp();
    };

    BackHandler.addEventListener('hardwareBackPress', handleBack);
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
  }, []);

  const getView = async () => {
    const {view} = await useGetObject(activeUser);

    if (view === 'grid') return setIsGridView(true);
  };

  const handleView = async () => {
    const userData = await useGetObject(activeUser);

    await useSetObject(activeUser, {
      name: userData.name,
      tracklist: userData.tracklist,
      view: !isGridView ? 'grid' : 'list',
    });

    setIsGridView(!isGridView);
  };

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />

      <TabBar
        left={
          <Text
            numberOfLines={1}
            style={{
              fontSize: normalize(18),
              fontWeight: '700',
              color: 'black',
              textTransform: 'capitalize',
            }}>{`Hi, ${activeUser}`}</Text>
        }
        center={
          <Image
            source={require('../assets/images/logo.png')}
            style={{
              aspectRatio: 2 / 1,
              height: normalize(30),
              tintColor: 'black',
            }}
          />
        }
        right={
          <TouchableOpacity onPress={() => handleView()}>
            {!isGridView ? (
              <Ionicons
                name="grid-outline"
                size={normalize(25)}
                color={'black'}
              />
            ) : (
              <Ionicons
                name="list-outline"
                size={normalize(25)}
                color={'black'}
              />
            )}
          </TouchableOpacity>
        }
      />

      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          flexWrap: 'wrap',
          flexDirection: 'row',
          paddingVertical: normalize(10),
        }}>
        {data.map((item, index) => (
          <Card
            item={item}
            key={index}
            isGridView={isGridView}
            onPress={() => navigation.navigate('Event', {...item})}
          />
        ))}
      </ScrollView>
    </View>
  );
};

export default Home;
