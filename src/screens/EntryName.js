import React, {useState, useContext, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {useDimensions} from '@react-native-community/hooks';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import normalize from 'react-native-normalize';
import {AuthContext} from '../contexts';
import CustomButton from '../components/atoms/CustomButton';
import {useClearAll, useGetAllKeys} from '../hooks';
import {SafeAreaView} from 'react-native-safe-area-context';

const EntryName = () => {
  const {saveName} = useContext(AuthContext);
  const {width, height} = useDimensions().window;
  const [name, setName] = useState('');

  const [history, setHistory] = useState(false);
  const [allKey, setAllKey] = useState([]);

  const handleGetAllKey = async () => {
    setAllKey(await useGetAllKeys());
  };

  const handleSaveName = () => {
    saveName(name);
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      contentContainerStyle={{
        flex: 1,
        paddingTop: normalize(30),
      }}>
      <StatusBar translucent backgroundColor="transparent" />

      <Image
        source={require('../assets/images/pexels-laura-stanley-2147029.jpg')}
        style={{
          resizeMode: 'cover',
          height: height + (height * 10) / 100,
          width,
          position: 'absolute',
        }}
      />

      <Image
        source={require('../assets/images/logo.png')}
        style={{
          aspectRatio: 2 / 1,
          marginTop: normalize(100),
          marginBottom: normalize(30),
          height: normalize(70),
          alignSelf: 'center',
          tintColor: '#dfdfdf',
        }}
      />

      <View
        style={{
          marginHorizontal: normalize(50),
          justifyContent: 'center',
          padding: normalize(20),
        }}>
        <Text style={{color: 'white'}}>Tell Me Your Name</Text>
        <TextInput
          selectionColor={'white'}
          onChangeText={(text) => setName(text.trim())}
          style={{
            borderWidth: 0.5,
            borderColor: 'white',
            height: normalize(40),
            color: 'white',
            fontSize: normalize(18),
            padding: normalize(10),
            marginTop: normalize(10),
            borderRadius: normalize(7),
          }}
          maxLength={20}
        />
        <TouchableOpacity
          disabled={name === '' && true}
          onPress={() => handleSaveName()}
          activeOpacity={0.7}
          style={{
            aspectRatio: 1,
            marginTop: normalize(20),
            height: normalize(50),
            borderRadius: normalize(50),
            backgroundColor: name.length > 0 ? 'white' : '#dddddd',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <MaterialCommunityIcons
            name="arrow-right"
            color={name.length > 0 ? 'black' : 'rgba(0,0,0,0.3)'}
            size={normalize(25)}
          />
        </TouchableOpacity>
      </View>

      <TouchableOpacity
        onPress={() => {
          handleGetAllKey();
          setHistory(!history);
        }}
        style={{
          alignSelf: 'center',
          borderWidth: 0.5,
          borderColor: 'white',
          marginTop: normalize(50),
          marginBottom: normalize(20),
          backgroundColor: 'rgba(0,0,0,0.2)',
          paddingHorizontal: normalize(10),
          paddingVertical: normalize(5),
          borderRadius: normalize(5),
        }}>
        <Text style={{color: 'white'}}>History</Text>
      </TouchableOpacity>

      {/* history panel */}
      {history && (
        <ScrollView
          contentContainerStyle={{
            backgroundColor: 'rgba(0,0,0,0.2)',
            marginHorizontal: normalize(50),
            padding: normalize(10),
          }}>
          {allKey
            ?.filter((f) => f !== 'activeUser')
            .map((e, i) => (
              <Text key={i} style={{color: 'white'}}>
                {i + 1}. {e}
              </Text>
            ))}
          <CustomButton
            text={'Clear All'}
            backgroundColor={'red'}
            rounded="l"
            height={30}
            fontSize={15}
            onPress={() => {
              useClearAll();
              setHistory(!history);
            }}
            style={{marginTop: normalize(20)}}
          />
        </ScrollView>
      )}
    </ScrollView>
  );
};

export default EntryName;
