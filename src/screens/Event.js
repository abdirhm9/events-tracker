import {useDimensions} from '@react-native-community/hooks';
import React, {useEffect, useState} from 'react';
import {View, Text, Image} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import normalize from 'react-native-normalize';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CustomButton from '../components/atoms/CustomButton';
import CustomText from '../components/atoms/CustomText';
import Thumbnail from '../components/atoms/Thumbnail';
import {
  useAddTracklist,
  useGetObject,
  useRemoveTracklist,
  useSetObject,
} from '../hooks';

const Event = ({route}) => {
  const {width} = useDimensions().window;
  const {title, thumbnail, date, venue, price, activeUser} = route.params;
  const [alreadyTracked, setAlreadyTracked] = useState(false);

  useEffect(() => {
    const getTracklist = async () => {
      const {tracklist} = await useGetObject(activeUser);

      if (tracklist.length > 0) {
        const track = tracklist.find((e) => e.title === title);

        if (track) return setAlreadyTracked(true);

        return;
      }
    };

    getTracklist();
  }, []);

  const handleAddTracklist = async () => {
    await useAddTracklist(activeUser, {title, thumbnail, date, venue, price});

    setAlreadyTracked(true);
  };

  const handleRemoveTracklist = async () => {
    await useRemoveTracklist(activeUser, title);

    setAlreadyTracked(false);
  };

  return (
    <ScrollView contentContainerStyle={{backgroundColor: 'white', flex: 1}}>
      <Thumbnail height={width / 2} source={thumbnail} />
      <View style={{padding: normalize(15)}}>
        <CustomText
          text={date}
          size={'xl'}
          color={'red'}
          bold
          contentContainerStyle={{marginBottom: normalize(10)}}
        />
        <CustomText text={title} size={'2xl'} color={'black'} bold />
        <CustomText
          text={venue}
          size={'l'}
          contentContainerStyle={{marginBottom: normalize(20)}}
        />
        {price > 0 ? (
          <CustomText text={'$' + price} size={'3xl'} color={'green'} bold>
            <Ionicons
              name="pricetags-outline"
              color={'green'}
              size={normalize(20)}
            />
          </CustomText>
        ) : (
          <CustomText text={'FREE'} size={'3xl'} color={'green'} bold>
            <Ionicons
              name="pricetags-outline"
              color={'green'}
              size={normalize(20)}
            />
          </CustomText>
        )}
      </View>

      <View
        style={{
          height: normalize(70),
          backgroundColor: 'white',
          elevation: 5,
          position: 'absolute',
          bottom: 0,
          width,
          justifyContent: 'center',
          paddingHorizontal: normalize(50),
        }}>
        {alreadyTracked ? (
          <CustomButton
            rounded={'full'}
            borderColor={'red'}
            text={'Remove'}
            onPress={() => handleRemoveTracklist()}
          />
        ) : (
          <CustomButton
            rounded={'full'}
            backgroundColor={'red'}
            text={'Track Event Now'}
            onPress={() => handleAddTracklist()}
          />
        )}
      </View>
    </ScrollView>
  );
};

export default Event;
