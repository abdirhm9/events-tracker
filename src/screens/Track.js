import React, {useCallback, useState} from 'react';
import {View, Text, TouchableOpacity, BackHandler} from 'react-native';
import TabBar from '../components/organisms/TabBar';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import normalize from 'react-native-normalize';
import {FlatList} from 'react-native-gesture-handler';
import {useGetObject, useRemoveTracklist} from '../hooks';
import {useFocusEffect} from '@react-navigation/native';
import List from '../components/organisms/List';
import Empty from '../components/organisms/Empty';

const Track = ({navigation, route}) => {
  const {activeUser} = route.params;

  const [data, setData] = useState([]);

  useFocusEffect(
    useCallback(() => {
      let mounted = true;

      mounted && main();

      return () => (mounted = false);
    }, []),
  );

  const main = async () => {
    await handleSetData();
  };

  const handleSetData = async () => {
    const {tracklist} = await useGetObject(activeUser);

    setData(tracklist);
  };

  const handleRemoveTracklist = async ({title}) => {
    await useRemoveTracklist(activeUser, title);

    handleSetData();
  };

  const renderItem = ({item}) => {
    return <List item={item} onPress={() => handleRemoveTracklist(item)} />;
  };

  return (
    <View style={{flex: 1}}>
      <TabBar
        left={
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <MaterialCommunityIcons name="arrow-left" size={normalize(25)} />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: normalize(20),
                marginLeft: normalize(20),
                fontWeight: '600',
              }}>
              Tracklist
            </Text>
          </View>
        }
      />

      {data.length > 0 ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data.reverse()}
          renderItem={renderItem}
          keyExtractor={(item) => item.title}
        />
      ) : (
        <Empty text={'Tracklist Is Empty'} />
      )}
    </View>
  );
};

export default Track;
