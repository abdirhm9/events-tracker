import React from 'react';
import {View} from 'react-native';
import normalize from 'react-native-normalize';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomText from '../atoms/CustomText';

const Empty = ({text = 'Empty!'}) => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <CustomText text={text} color={'darkgrey'} size={'m'}>
        <MaterialCommunityIcons
          name="alert-box-outline"
          size={normalize(25)}
          color={'darkgrey'}
        />
      </CustomText>
    </View>
  );
};

export default Empty;
