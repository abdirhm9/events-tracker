import {useDimensions} from '@react-native-community/hooks';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import normalize from 'react-native-normalize';
import {data} from '../../utils/Data';
import CustomButton from '../atoms/CustomButton';
import CustomText from '../atoms/CustomText';
import Thumbnail from '../atoms/Thumbnail';

const List = ({item = {}, onPress = () => {}}) => {
  const navigation = useNavigation();
  const {width} = useDimensions().window;

  return (
    <View
      style={{
        backgroundColor: 'white',
        marginBottom: normalize(15),
        overflow: 'hidden',
        padding: normalize(10),
        elevation: 2,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: normalize(10),
          borderBottomWidth: 0.5,
          paddingBottom: normalize(10),
          borderColor: 'lightgray',
        }}>
        <CustomText text={item.title} size={'l'} bold color={'black'} />
        <CustomText text={item.date} size={'l'} bold color={'red'} />
      </View>

      <Thumbnail
        source={item.thumbnail}
        height={normalize(100)}
        aspectRatio={null}
      />

      <View style={{marginTop: normalize(10), flexDirection: 'row'}}>
        <CustomButton
          backgroundColor={'green'}
          text={'Event Info'}
          height={30}
          rounded={'s'}
          style={{flex: 2}}
          fontBold={false}
          fontSize={18}
          onPress={() => navigation.navigate('Event', {...item})}
        />
        <CustomButton
          borderColor={'red'}
          text={'Remove'}
          height={30}
          rounded={'s'}
          style={{flex: 1, marginLeft: normalize(10)}}
          fontBold={false}
          fontSize={18}
          onPress={onPress}
        />
      </View>
    </View>
  );
};

export default List;
