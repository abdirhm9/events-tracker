import React from 'react';
import {View, Text} from 'react-native';
import normalize from 'react-native-normalize';

const TabBar = ({left, center = null, right = null}) => {
  return (
    <View
      style={{
        height: normalize(50),
        backgroundColor: 'white',
        elevation: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: normalize(20),
      }}>
      <View style={{flex: 1, alignItems: 'flex-start'}}>{left}</View>
      <View style={{flex: 1, alignItems: 'center'}}>{center}</View>
      <View style={{flex: 1, alignItems: 'flex-end'}}>{right}</View>
    </View>
  );
};

export default TabBar;
