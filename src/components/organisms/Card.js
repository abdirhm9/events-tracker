import {useDimensions} from '@react-native-community/hooks';
import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import normalize from 'react-native-normalize';
import CustomText from '../atoms/CustomText';
import Thumbnail from '../atoms/Thumbnail';

const Card = ({item = {}, isGridView = false, onPress = () => {}}) => {
  const {width} = useDimensions().window;

  const getPrice = ({price}) => {
    if (price < 100) return '$';
    if (price >= 100 && price <= 500) return '$$';

    return '$$$';
  };

  return (
    <View
      style={{
        width: isGridView ? width / 2 : width,
        padding: normalize(8),
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => onPress()}
        style={[
          {
            borderRadius: normalize(10),
            overflow: 'hidden',
            backgroundColor: 'white',
            elevation: 2,
          },
          isGridView && {height: normalize(230)},
        ]}>
        <Thumbnail
          source={item.thumbnail}
          height={isGridView ? width / 4 : width / 2}
        />
        <View
          style={{
            paddingHorizontal: normalize(10),
            paddingTop: normalize(10),
            paddingBottom: normalize(20),
          }}>
          <CustomText
            text={item.date}
            size={isGridView ? 's' : 'l'}
            color={'red'}
            bold
            contentContainerStyle={{marginBottom: normalize(10)}}
          />
          <CustomText
            text={item.title}
            color={'black'}
            size={isGridView ? 'l' : 'xl'}
            bold
          />
          <CustomText
            text={item.venue}
            color={'darkgrey'}
            size={isGridView ? 's' : 'm'}
            contentContainerStyle={{marginBottom: normalize(15)}}
          />
          {item.price > 0 ? (
            <CustomText
              text={getPrice(item)}
              size={isGridView ? 'm' : 'l'}
              color={'green'}
              bold
            />
          ) : (
            <CustomText
              text={'FREE'}
              size={isGridView ? 'm' : 'l'}
              color={'green'}
              bold
            />
          )}
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Card;
