import React, {useState} from 'react';
import {Text, View} from 'react-native';
import normalize from 'react-native-normalize';

const CustomText = ({
  text = 'TBA',
  size = 'm',
  color = 'grey',
  bold = false,
  contentContainerStyle = {},
  textStyle = {},
  children,
}) => {
  const getFontSize = () => {
    if (size.toLowerCase() === 's') return normalize(12);
    if (size.toLowerCase() === 'l') return normalize(18);
    if (size.toLowerCase() === 'xl') return normalize(21);
    if (size.toLowerCase() === '2xl') return normalize(24);
    if (size.toLowerCase() === '3xl') return normalize(27);
    if (size.toLowerCase() === '4xl') return normalize(30);

    return normalize(15);
  };

  return (
    <View
      style={[
        {
          flexDirection: 'row',
          alignItems: 'center',
        },
        {...contentContainerStyle},
      ]}>
      <View style={{marginRight: children ? normalize(10) : 0}}>
        {children}
      </View>
      <Text
        style={[
          {
            fontSize: getFontSize(),
            color,
            fontWeight: bold ? 'bold' : 'normal',
          },
          {...textStyle},
        ]}>
        {text}
      </Text>
    </View>
  );
};

export default CustomText;
