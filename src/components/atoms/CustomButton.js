import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import normalize from 'react-native-normalize';

function CustomButton({
  textStyle = {},
  style = {},
  backgroundColor = null,
  height = 50,
  width = '100%',
  rounded = null,
  text = 'Your Text Here!',
  borderColor = null,
  color = 'white',
  fontSize = 20,
  fontBold = true,
  onPress = () => {},
}) {
  const getBorderRadius = () => {
    if (rounded?.toLowerCase() === 's') return normalize(height / 10);
    if (rounded?.toLowerCase() === 'm') return normalize(height / 8);
    if (rounded?.toLowerCase() === 'l') return normalize(height / 6);
    if (rounded?.toLowerCase() === 'xl') return normalize(height / 4);
    if (rounded?.toLowerCase() === 'full') return normalize(height);

    return normalize(0);
  };

  return (
    <TouchableOpacity
      onPress={() => onPress()}
      activeOpacity={0.7}
      style={[
        {
          backgroundColor,
          height: normalize(height),
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: getBorderRadius(),
          borderColor,
          borderWidth: borderColor ? 1 : 0,
        },
        {...style},
      ]}>
      <Text
        style={[
          {
            color: borderColor ? borderColor : color,
            fontSize: normalize(fontSize),
            fontWeight: fontBold ? '700' : '600',
          },
          {...textStyle},
        ]}>
        {text}
      </Text>
    </TouchableOpacity>
  );
}

export default CustomButton;
