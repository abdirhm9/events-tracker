import React from 'react';
import {Image} from 'react-native';

const Thumbnail = ({
  source,
  height = '100%',
  width = '100%',
  aspectRatio = 2 / 1,
  style,
}) => {
  return (
    <Image
      style={[style, {resizeMode: 'cover', aspectRatio, height, width}]}
      source={source}
    />
  );
};

export default Thumbnail;
