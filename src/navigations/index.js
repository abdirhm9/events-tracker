import React, {useEffect, useMemo, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {AuthContext} from '../contexts';
import Home from '../screens/Home';
import Track from '../screens/Track';
import Event from '../screens/Event';
import EntryName from '../screens/EntryName';
import {
  useGetAllKeys,
  useGetItem,
  useGetObject,
  useSetItem,
  useSetObject,
} from '../hooks';

export const Navigation = () => {
  const [isAuth, setIsAuth] = useState(false);
  const [name, setName] = useState('');
  const [activeUser, setActiveUser] = useState('');

  useEffect(() => {
    let mounted = true;

    const main = async () => {
      const obj = await useGetObject(name);

      // First Login
      if (!obj && name.length > 0) {
        const initialObject = {
          name,
          tracklist: [],
          view: 'list',
        };

        await useSetObject(name, initialObject);

        await useSetItem('activeUser', name);

        setActiveUser(name);

        return setIsAuth(true);
      }

      // Recognized User
      if (obj?.name === name) {
        await useSetItem('activeUser', name);

        setActiveUser(name);

        return setIsAuth(true);
      }

      return;
    };

    mounted && main();

    return () => (mounted = false);
  }, [name, activeUser]);

  const authContext = useMemo(() => {
    return {
      saveName: async (data) => {
        setName(data.toLowerCase());
      },
    };
  }, []);

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen isAuth={isAuth} activeUser={activeUser} />
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

const TabStack = createMaterialTopTabNavigator();
const TabStackScreen = ({route}) => {
  return (
    <TabStack.Navigator tabBarOptions={{style: {display: 'none'}}}>
      <TabStack.Screen
        initialParams={{...route.params}}
        name="Home"
        component={Home}
      />
      <TabStack.Screen
        initialParams={{...route.params}}
        name="Track"
        component={Track}
        options={{unmountOnBlur: true}}
      />
    </TabStack.Navigator>
  );
};

const HomeStack = createStackNavigator();
const HomeStackScreen = ({route}) => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        initialParams={{...route.params}}
        name="TabStackScreen"
        component={TabStackScreen}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        initialParams={{...route.params}}
        name="Event"
        component={Event}
        options={({route}) => ({
          title: route.params.title,
          gestureEnabled: true,
          gestureDirection: 'horizontal',
          gestureResponseDistance: {horizontal: 100},
        })}
      />
    </HomeStack.Navigator>
  );
};

const AuthStack = createStackNavigator();
const AuthStackScreen = () => {
  return (
    <AuthStack.Navigator>
      <AuthStack.Screen
        name="Entry Name"
        component={EntryName}
        options={{headerShown: false}}
      />
    </AuthStack.Navigator>
  );
};

const RootStack = createStackNavigator();
const RootStackScreen = ({isAuth, activeUser}) => (
  <RootStack.Navigator headerMode="none">
    {isAuth ? (
      <RootStack.Screen
        initialParams={{activeUser}}
        name="HomeStackScreen"
        component={HomeStackScreen}
        options={{
          animationEnabled: false,
        }}
      />
    ) : (
      <RootStack.Screen
        name="AuthStackScreen"
        component={AuthStackScreen}
        options={{
          animationEnabled: false,
        }}
      />
    )}
  </RootStack.Navigator>
);
