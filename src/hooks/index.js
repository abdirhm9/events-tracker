import AsyncStorage from '@react-native-async-storage/async-storage';

export const useClearAll = async () => {
  try {
    await AsyncStorage.clear();
  } catch (e) {
    // clear error
  }

  console.log('useClearAll Done.');
};

export const useGetAllKeys = async () => {
  let keys = [];
  try {
    keys = await AsyncStorage.getAllKeys();
  } catch (e) {
    // read key error
  }

  return keys;
  // example console.log result:
  // ['@MyApp_user', '@MyApp_key']
};

export const useGetObject = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // read error
  }

  console.log('useGetObject Done.');
};

export const useSetObject = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    // save error
  }

  console.log('useSetObject Done.');
};

export const useSetItem = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    // save error
  }

  console.log('useSetItem Done.');
};

export const useGetItem = async (key) => {
  try {
    return await AsyncStorage.getItem(key);
  } catch (e) {
    // read error
  }

  console.log('useGetItem Done.');
};

export const useAddTracklist = async (activeUser, item) => {
  const userData = await useGetObject(activeUser);

  return await useSetObject(activeUser, {
    name: userData.name,
    tracklist: [...userData.tracklist, item],
    view: userData.view,
  });
};

export const useRemoveTracklist = async (activeUser, title) => {
  const userData = await useGetObject(activeUser);

  return await useSetObject(activeUser, {
    name: userData.name,
    tracklist: [...userData.tracklist.filter((e) => e.title !== title)],
    view: userData.view,
  });
};
